---
title: Merged with text.malam.or.id
date: 2024-03-16T01:16:42+07:00
tags:
  - weblog
---

This weblog has been merged with [main blog](https://text.malam.or.id).

Please visit [Spam Suck! new page](https://text.malam.or.id/spams/).
